#!/bin/bash

# Add Vault Repository
wget -qO - https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o $KEYRINGS_PATH/hashicorp-keyring.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/hashicorp-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" \
    | sudo tee /etc/apt/sources.list.d/hashicorp.list

# Update Repositories
sudo apt-get update

# Install Vault
sudo apt-get install -y vault




